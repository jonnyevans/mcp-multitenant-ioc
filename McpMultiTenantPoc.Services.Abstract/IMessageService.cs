﻿namespace McpMultiTenantPoc.Services.Abstract
{
    public interface IMessageService
    {
        string GetGreeting();
    }
}