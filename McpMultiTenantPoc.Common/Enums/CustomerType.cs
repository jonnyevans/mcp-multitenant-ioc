﻿namespace McpMultiTenantPoc.Common.Enums
{
    public enum CustomerType
    {
        Unspecified = 0,
        Asda = 1,
        Ahold = 2,
        Snow = 3,
        Walmart = 4
    }
}