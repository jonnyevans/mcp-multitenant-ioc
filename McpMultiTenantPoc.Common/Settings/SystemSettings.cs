﻿using McpMultiTenantPoc.Common.Enums;

namespace McpMultiTenantPoc.Common.Settings
{
    public class SystemSettings
    {
        public CustomerType CustomerType { get; }
        
        public SystemSettings(CustomerType customerType)
        {
            CustomerType = customerType;
        }
    }
}