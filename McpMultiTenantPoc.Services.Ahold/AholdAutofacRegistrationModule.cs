﻿using Autofac;
using McpMultiTenantPoc.Common.Enums;
using McpMultiTenantPoc.Common.Settings;
using McpMultiTenantPoc.Services.Abstract;

namespace McpMultiTenantPoc.Services.Ahold
{
    public class AholdAutofacRegistrationModule : Module
    {
        private readonly SystemSettings _systemSettings;

        public AholdAutofacRegistrationModule(SystemSettings settings)
        {
            _systemSettings = settings;
        }

        protected override void Load(ContainerBuilder builder)
        {
            if (_systemSettings.CustomerType != CustomerType.Ahold)
                return;

            builder.RegisterType<Ahold.MessageService>().As<IMessageService>();
        }
    }
}
