﻿namespace McpMultiTenantPoc.Services.Ahold
{
    public class MessageService : Core.MessageService
    {
        public override string GetGreeting()
        {
            return "Ahold only get a little greeting message :(";
        }
    }
}