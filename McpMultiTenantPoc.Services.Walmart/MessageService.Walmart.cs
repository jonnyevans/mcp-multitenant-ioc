﻿namespace McpMultiTenantPoc.Services.Walmart
{
    public class MessageService : Core.MessageService
    {
        public override string GetGreeting()
        {
            return "I am Walmarts extended greeting message.";
        }
    }
}