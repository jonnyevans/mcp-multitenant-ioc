﻿using Autofac;
using McpMultiTenantPoc.Common.Enums;
using McpMultiTenantPoc.Common.Settings;
using McpMultiTenantPoc.Services.Abstract;

namespace McpMultiTenantPoc.Services.Walmart
{
    public class WalmartAutofacRegistrationModule : Module
    {
        private readonly SystemSettings _systemSettings;

        public WalmartAutofacRegistrationModule(SystemSettings settings)
        {
            _systemSettings = settings;
        }

        protected override void Load(ContainerBuilder builder)
        {
            if (_systemSettings.CustomerType != CustomerType.Walmart)
                return;

            builder.RegisterType<Walmart.MessageService>().As<IMessageService>();
        }
    }
}