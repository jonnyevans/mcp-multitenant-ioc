﻿using Autofac;
using McpMultiTenantPoc.Services.Abstract;

namespace McpMultiTenantPoc.Console
{
    internal class Program
    {
        private static void Main()
        {
            var systemSettings = SettingsConfig.Build();
            var container = DependencyConfig.Build(systemSettings);

            var messageService = container.Resolve<IMessageService>();
            var greeting = messageService.GetGreeting();
            
            System.Console.WriteLine(greeting);
            System.Console.ReadKey();
        }
    }
}