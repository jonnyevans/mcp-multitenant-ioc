﻿using System;
using System.Configuration;
using McpMultiTenantPoc.Common.Enums;
using McpMultiTenantPoc.Common.Settings;

namespace McpMultiTenantPoc.Console
{
    public class SettingsConfig
    {
        public static SystemSettings Build()
        {
            var customer = ConfigurationManager.AppSettings.Get("Customer");
            Enum.TryParse(customer, out CustomerType customerType);

            return new SystemSettings(customerType);
        }
    }
}