﻿using Autofac;
using McpMultiTenantPoc.Common.Settings;
using McpMultiTenantPoc.Services.Ahold;
using McpMultiTenantPoc.Services.Core;
using McpMultiTenantPoc.Services.Walmart;

namespace McpMultiTenantPoc.Console
{
    public static class DependencyConfig
    {
        public static IContainer Build(SystemSettings settings)
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule(new CoreAutofacRegistrationModule());
            builder.RegisterModule(new AholdAutofacRegistrationModule(settings));
            builder.RegisterModule(new WalmartAutofacRegistrationModule(settings));

            return builder.Build();
        }
    }
}