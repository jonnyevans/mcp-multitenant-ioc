﻿using Autofac;
using McpMultiTenantPoc.Services.Abstract;

namespace McpMultiTenantPoc.Services.Core
{
    public class CoreAutofacRegistrationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Core.MessageService>().As<IMessageService>();
        }
    }
}