﻿using McpMultiTenantPoc.Services.Abstract;

namespace McpMultiTenantPoc.Services.Core
{
    public class MessageService : IMessageService
    {
        public virtual string GetGreeting()
        {
            return "I am the Core greeting set by default.";
        }
    }
}